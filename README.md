# STL Data Engineering Technical Interview Guide

This is the technical screen for candidates looking to work for STL Data Engineering. This screening is meant to assess the technical fit with our Slalom STL team, and it does not cover assessing consulting/client-facing skills or team dynamics.

## General Interview

- Talent Acquisition performs initial screening
- A technical screening is performed by an engineer that has taken the Behavioral Interview Training. If you haven't taken this yet, please reach out to Dan Kieffer in TA to get this arranged.
- The PL meets with the candidate and uses the questions from the Me@Slalom guide to assess consulting.
- The Data Engineering practice lead meets with the candidate for final review.

### Language Prereqs

Given that SQL is the _lingua franca_ of the data world, the SQL requirement is non-negotiable.

At a minimum, candidates are required to have SQL and Python in their toolbox as
the vast majority of data applications treat Python and SQL as first-class languages. The Python requirement may be waved if and only if the candidate has demonstrated experience in another GPL commonly used in developing data infrastructure or interacting with data applications (e.g., JavaScript/TypeScript, Golang, Scala, Java, C#/.NET). In the event of this happening, the candidate can use one of these languages to complete the technical screen.

Live-coding can be intimidating, so help the candidate feel relaxed if you start to sense excessive stress. Encourage them to communicate their ideas and logic when approaching a problem. We want thinkers before developers.

### Knowledge

The following can be treated as a picklist that is up to the interviewers discretion to cover, with asterisked items being ones that are fundamental to Consultant and higher:

- __Datastores__
    + OLAP and OLTP style databases *
    + Object storage *
    + Document/Graph DBs
- __Data Processing__
    + Batch and streaming *
    + Message queues
    + MapReduce-based frameworks *
    + Change Data Capture
- __Data Warehousing__
    + Snowflake and Star Schemas *
    + Dimensional modeling *
    + [Functional data warehousing](https://maximebeauchemin.medium.com/functional-data-engineering-a-modern-paradigm-for-batch-data-processing-2327ec32c42a)
- API development
- Data Structures (keep it relatively independent of the language) *
- Algorithms
- Security best practices *
- Networking


## Python & SQL Challenge Ratings

Please add your challenge to the table below and give it a ranking.

| Directory Path | Name | Language | Focus | Rating (Beginner/Intermediate/Advanced)|
| ---------------| ---- |  ------- | ------- | ------|
| `python/fizz_buzz` | Fizz Buzz | Python | Control Flow | Beginner |
| `python/calculate_median` | Calculate Median | Python | Data Structures | Beginner-Intermediate |
| `python/create_lookup`    | Food Finder      | Python | Data Structures, Exception Handling | Beginner-Intermediate |

## Contributions

Please contribute! There's plenty of work to be done here, but many hands will make light of it. If you would like to contribute, please reach out to me via email or Teams.

## TODO

- Add SQL screen
- Add system design screen
