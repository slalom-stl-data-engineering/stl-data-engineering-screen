"""
Find the Median

Problem:
    Given an array, `nums`, of integers with n-length,
    calculate the median value of
    that array.

Assumptions:
    - Array length is n >= 1
    - Integers may not be in order
    - Integers may be positive or negative

Examples:

    In the case of lists where there is no immediate median value,
the two most-centered values are averaged to create the median.

>> nums = [1, 3, 5, 7]
>> median(nums)
4

>> nums = [1, 3, 5, 7, 9]
>> median(nums)
5

Restrictions:
    - No use of other libraries (e.g., `statistics.median`)

"""
from typing import List


def median(nums: List[int]) -> int:
    pass
