from statistics import median
import pytest
from hypothesis import strategies, given
import calculate_median


def solution(nums: list) -> int:
    nums.sort()
    if len(nums) % 2 == 0:
        pivot = int(len(nums) / 2)
        pointer1 = nums[pivot]
        pointer2 = nums[pivot - 1]
        median_val = (pointer1 + pointer2) / 2
        return median_val
    else:
        array_len = len(nums)
        pivot = int((len(nums) + 1) / 2)
        index = array_len - pivot
        return nums[index]


@given(strategies.lists(strategies.integers(), min_size=1))
def test_solution(nums):
    assert median(nums) == solution(nums)


@given(strategies.lists(strategies.integers(), min_size=1))
def test_median(nums):
    assert calculate_median.median(nums) == median(nums)
