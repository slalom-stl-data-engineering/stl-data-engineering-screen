from typing import Union
import pytest


# Create a test case, solution, and test it
def arrays():
    animal = ["lion", "tiger", "bear"]
    food = [
        ["gazelle", "impala"],
        ["water buffalo", "antelope", "wild boar"],
        ["salmon", "deer"],
    ]
    return animal, food


def solution(animal: str) -> Union[str, list]:
    animal_foods = arrays()
    full: dict = {key: value for key, value in zip(
        animal_foods[0], animal_foods[1])}
    try:
        return full[animal]
    except KeyError as e:
        raise Exception(f"{animal} not found!") from e


def test_solution():
    assert solution("lion") == ["gazelle", "impala"]
    with pytest.raises(Exception):
        assert solution("foo")
