"""

Food Finder!

Using the two arrays below, `animal` and `food`, write a function that returns
the food of the animal given the animal's name as an input.

Example:
>>> animal = ["lion", "tiger", "bear"]
>>> food = [
        ["gazelle", "impala"],
        ["water buffalo", "antelope", "wild boar"],
        ["salmon", "deer"]
    ]
>>> food_finder("bear")
["salmon", "deer"]

Assumptions/Extras:
    - `animal` and `food` are already in the correct order.
    - If an animal is provided that's not in the `animal` array, throw an error
    with a custom message of "(the animal's name) not found!"

"""
from typing import Union


def food_finder(animal: str) -> Union[str, list]:
    pass
