"""
Fizz Buzz

Write a function that takes an array of integers and applies
the following rules to each element:
    - If the element is divisible by 3, replace the element with "Fizz"
    - If the element is divisible by 5, replace the element with "Buzz"
    - If the element is divisible by both 3 and 5, replace the element with "FizzBuzz"
    - If any of the above rules don't apply to the element, then do not modify the element

Example:
>> nums = [7, 8, 9, 10, 11, 12, 13, 14, 15]
>> fizz_buzz(nums)
[7, 8, "Fizz", "Buzz", 11, "Fizz", 13, 14, "FizzBuzz"]

Assumptions:
    - The array's length will always be greater than 1.
"""


def fizz_buzz(nums: list) -> list:
    pass
