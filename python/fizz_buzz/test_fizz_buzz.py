from hypothesis import strategies, given
import pytest
import fizz_buzz


def solution(nums: list) -> list:
    out = []
    for i in range(0, len(nums)):
        three: bool = nums[i] % 3 == 0
        five: bool = nums[i] % 5 == 0
        fifteen: bool = nums[i] % 15 == 0
        if fifteen:
            out.append("FizzBuzz")
        elif three:
            out.append("Fizz")
        elif five:
            out.append("Buzz")
        else:
            out.append(nums[i])
    return out


@pytest.fixture
def expected():
    return [
        1,
        2,
        "Fizz",
        4,
        "Buzz",
        "Fizz",
        7,
        8,
        "Fizz",
        "Buzz",
        11,
        "Fizz",
        13,
        14,
        "FizzBuzz",
        16,
        17,
        "Fizz",
        19,
        "Buzz",
        "Fizz",
        22,
        23,
        "Fizz",
        "Buzz",
        26,
        "Fizz",
        28,
        29,
        "FizzBuzz",
        31,
        32,
        "Fizz",
        34,
        "Buzz",
        "Fizz",
        37,
        38,
        "Fizz",
        "Buzz",
        41,
        "Fizz",
        43,
        44,
        "FizzBuzz",
        46,
        47,
        "Fizz",
        49,
        "Buzz",
    ]


def test_solution(expected):
    assert solution([i for i in range(1, 51)]) == expected


@given(strategies.lists(strategies.integers(), min_size=1))
def test_fizz_buzz(nums):
    expected = solution(nums)
    assert fizz_buzz.fizz_buzz(nums) == expected
